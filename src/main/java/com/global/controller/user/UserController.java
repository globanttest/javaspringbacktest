package com.global.controller.user;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.global.model.user.Usuario;
import com.global.service.UserService;

@RestController
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@PostMapping(path = "/user", consumes = "application/json")
	public ResponseEntity postUser(@Valid @RequestBody Usuario usuario) {
		try {
			return userService.postUser(usuario);
		} catch (Exception e) {
			logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
			return null;
		}
	}

	@GetMapping(path = "/user/list",produces = "application/json")
	public List<Usuario> listUsuario(){
		try {

			return userService.listUser();
		} catch (Exception e) {
			logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
			return null;
		}
	}
	@GetMapping(path = "/user",produces = "application/json")
	public Usuario getUser(@RequestParam(required = true, name = "id") Integer id) {
		try {

			return userService.getUser(id);
		} catch (Exception e) {
			logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
			return null;
		}
	}
}
