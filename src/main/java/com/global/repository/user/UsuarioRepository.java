package com.global.repository.user;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.global.model.user.Usuario;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

	@Override
	public List<Usuario> findAll();

}
