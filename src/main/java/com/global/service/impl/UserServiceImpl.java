package com.global.service.impl;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.global.model.user.Usuario;
import com.global.repository.user.UsuarioRepository;
import com.global.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public ResponseEntity postUser(Usuario usuario) throws Exception {
		try {
			usuarioRepository.save(usuario);
			return new ResponseEntity<Usuario>(HttpStatus.CREATED);
		} catch (Exception e) {
			logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
			throw e;
		}
	}

	@Override
	public List<Usuario> listUser() throws Exception {
		try {
			List<Usuario> listUsuario;
			listUsuario = usuarioRepository.findAll();
			return listUsuario;
		} catch (Exception e) {
			logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
			throw e;
		}
	}

	@Override
	public Usuario getUser(Integer id) throws Exception {
		try {
			Optional<Usuario> usuarioOpt;
			usuarioOpt = usuarioRepository.findById(id);
			if(usuarioOpt.isPresent()) {
				return usuarioOpt.get();
			}else {
				throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encuentra informacion para la solicitud realizada");
			}
		} catch (Exception e) {
			logger.error(Thread.currentThread().getStackTrace()[1].getMethodName(), e);
			throw e;
		}
	}

}
