package com.global.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.global.model.user.Usuario;

public interface UserService {

	public ResponseEntity postUser(Usuario usuario)throws Exception;

	public List<Usuario> listUser()throws Exception;

	public Usuario getUser(Integer id) throws Exception;

}
